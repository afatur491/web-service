<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

header("Access-Control-Allow-Origin: * ");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Auth, X-Requested-With");

class Api extends RestController
{
	public function __construct()
	{
		parent::__construct();

		$this->getMiddleware()->run();
	}

	public function index_get()
	{
		// var_dump($this->getMiddleware()->read());
		// $res = $this->res->send(1, ' API Work !');
		$res = $this->sess->get();
		$this->response($res, 200);
		// $this->response($this->currentUser(), 200);
	}
}
