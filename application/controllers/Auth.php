<?php

defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Auth extends RestController
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_get()
	{
		$this->response($this->res->send(0, 'Not Allowed'), 400);
	}

	public function register_post()
	{
		$data['nama'] = $this->input->post('nama');
		$data['email'] = $this->input->post('email');
		$password = $this->input->post('password');
		$data['password'] = sha1($password);
		$rePass = $this->input->post('rePass');

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email[users.email]');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
		$this->form_validation->set_rules('rePass', 'Password konfirmasi', 'required|matches[password]');

		if ($this->form_validation->run() == false) {
			$res = $this->res->send(0, array_values($this->form_validation->error_array())[0]);
			return $this->response($res, 200);
		}

		if ($this->User_model->get_by_email_count($data['email']) > 0) {
			$res = $this->res->send(0, 'Email sudah terdaftar, coba yang lain !');
			return $this->response($res, 200);
		}

		$idUser = $this->User_model->create($data);

		$data['id'] = $idUser;

		$jwt_token = new JWT_token();
		$jwt_token->init([
			'id' => $idUser,
			'nama' => $data['nama'],
			'email' => $data['email']
		]);
		$token = $jwt_token->get_token();

		$this->User_model->update($idUser, $token);

		$res = $this->res->send(1, 'Berhasil mendaftar, silahkan login untuk melanjutkan!');
		return $this->response($res, 200);
	}

	public function login_post()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$getUser = $this->User_model->get_by_email($email);

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');

		if ($this->form_validation->run() == false) {
			$res = $this->res->send(0, array_values($this->form_validation->error_array())[0]);
			return $this->response($res, 200);
		}

		if (empty($getUser)) {
			$res = $this->res->send(0, 'Email dan password salah, silahkan coba lagi !');
			return $this->response($res, 200);
		}

		if ($getUser['password'] != sha1($password)) {
			$res = $this->res->send(0, 'Email dan password salah, silahkan coba lagi !');
			return $this->response($res, 200);
		}

		$res = $this->res->send(1, 'Berhasil login', [
			'token' => $getUser['token'],
		]);

		$this->sess->set([
			'isAuth' => 1,
			'data' => $getUser
		]);

		$this->response($res, 200);
	}
}