<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_model extends CI_Model
{
    private $table = 'products';
    public function __construct()
    {
        parent::__construct();
        $this->db = new PDO("mysql:host=localhost;dbname=eco-mercy", "root", "");
    }
    function getProduk($id, $idUser)
    {
        if ($id == null) {

            $dapat = $this->db->prepare("SELECT * FROM $this->table WHERE id_user=$idUser");
            $dapat->execute();
            $product = $dapat->fetchAll();
        } else {
            $dapat = $this->db->prepare("SELECT * FROM $this->table WHERE id=$id and id_user=$idUser");
            $dapat->execute();
            $product = $dapat->fetch();
        }
        return $product;
    }

    public function insertProduk($data)
    {
        extract($data);
        $insert = $this->db->prepare("INSERT INTO $this->table VALUE('','$nama','$deskripsi','$harga','$gambar','$id_user')");
        $insert->execute();
    }

    public function cekId($id, $idUser)
    {
        $cek = $this->db->prepare("SELECT * FROM $this->table WHERE id=$id and id_user=$idUser");
        $cek->execute();
        return $cek->rowCount();
    }

    public function updateProduct($id, $data)
    {
        extract($data);
        $update = $this->db->prepare("UPDATE $this->table SET nama='$nama',deskripsi='$deskripsi',harga='$harga',gambar='$gambar' WHERE id='$id'");
        $update->execute();
    }

    public function deleteProduct($id, $idUser)
    {
        $hapus = $this->db->prepare("DELETE FROM $this->table WHERE id=$id and id_user=$idUser");
        $hapus->execute();
    }
}