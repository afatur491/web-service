<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    private $table = 'users';
    public function __construct()
    {
        parent::__construct();
        $this->db = new PDO("mysql:host=localhost;dbname=eco-mercy", "root", "");
    }
    public function get_by_email_count($email)
    {
        $data = $this->db->prepare("SELECT * FROM users WHERE email='$email'");
        $data->execute();
        return $data->rowCount();
    }

    public function create($data)
    {
        extract($data);
        $insert = $this->db->prepare("INSERT INTO users VALUE ('',?,?,?,'')");
        $insert->execute([$nama, $email, $password]);
        return $this->db->lastInsertId();
    }

    public function get_by_email($email)
    {
        $data = $this->db->prepare("SELECT * FROM users WHERE email = '$email'");
        $data->execute();
        return $data->fetch();
    }

    public function update($id, $token)
    {
        $update = $this->db->prepare("UPDATE users SET token='$token' WHERE id='$id'");
        $update->execute();
    }

    public function get_token($email)
    {
        extract($this->get_by_email($email));
        return $token;
    }
    public function check_token($token)
    {
        $get = $this->db->prepare("SELECT * FROM users WHERE token=$token");
        $get->execute();
        $isget = $get->rowCount();
        return $isget > 0 ? true : false;
    }
}