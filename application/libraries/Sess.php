<?php

class Sess
{

    private $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('session');
    }

    public function get($name = '')
    {
        if (empty($name)) {
            return $this->CI->session->get_userdata();
        }
        return $this->CI->session->userdata($name);
    }

    public function set($key, $val = '')
    {
        if (is_array($key)) {
            return $this->CI->session->set_userdata($key);
        }

        return $this->CI->session->set_userdata($key, $val);
    }

    public function remove($key = '')
    {
        return $this->CI->session->unset_userdata($key);
    }

    public function isAuth()
    {
        return $this->get('isAuth') == 1 ? true : false;
    }
}
