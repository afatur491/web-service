<div id="content">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="section-judul">Token</h3>
                </div>
                <div class="col-lg-9 col-sm-10 col-10">
                    <p>Your Token:</p>
                </div>
                <div class="col-lg-3 col-sm-2 col-2">
                    <button id="copy" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="token copied!">copy</button>
                </div>
                <div class="col-lg-12">
                    <p id="token" class="token"><?= $token ?></p>
                </div>
            </div>
        </div>
    </section>
</div>