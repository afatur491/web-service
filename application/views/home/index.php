    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="<?=base_url()?>">Ecomercy</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ml-auto">
                    <a class="nav-item nav-link active" href="<?=base_url()?>">Home</a>
                    <a class="nav-item nav-link active" href="<?=base_url('doc/product')?>">Documentation</a>
                    <a class="nav-item nav-link btn btn-re" href="<?=base_url() ?>home/login">Getting Start</a>
                </div>
            </div>
        </div>
    </nav>
    <div class="jumbotron jumbotron-fluid">
        <div class="container">   
            <h1 class="display-4">EcoMercy API</h1>
            <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus, ab.</p>
            <a class="btn btn-orange btn-lg bulet" href="<?=base_url()?>home/login" role="button">Getting Start</a>
        </div>
    </div>
    <section class="intro">
        <div class="container bg-white">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <h2 class="text-center judul">INTRODUCING</h2>
                </div>
                <div class="col-lg-12 col-sm-12">
                    <p class="mt-3">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp    Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem ea quia deserunt voluptatum adipisci eum impedit ullam, molestiae itaque veritatis tempora saepe nisi quo corporis quisquam molestias ab sunt provident, totam quas nostrum suscipit consectetur! Mollitia dolor delectus atque. Reprehenderit, ea. Nulla perferendis facilis dicta suscipit sint ipsum corporis, aliquid iusto libero illum recusandae perspiciatis aspernatur? Quam atque fuga aspernatur et aut ut, consequuntur laborum error repellat in, sit aliquid?
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="service">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <h2 class="uppercase text-center judul">service</h2>
                </div>
                <div class="col-lg-12 col-sm-12">
                   <div class="row justify-content-center mt-4">
                       <div class="col-lg-4 col-sm-12 kart">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 text-center mt-4">
                                    <img src="<?=base_url('assets/img/buy.png') ?>" alt="product" class="img-ser">
                                    <h4 class="title-ser">CRUD PRODUCT</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus voluptatem accusantium delectus voluptatum adipisci commodi?</p>
                                </div>
                            </div>
                       </div>
                       <div class="col-lg-4 col-sm-12 kart">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 text-center mt-4">
                                    <img src="<?=base_url('assets/img/buy.png') ?>" alt="product" class="img-ser">
                                    <h4 class="title-ser">CRUD PRODUCT</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus voluptatem accusantium delectus voluptatum adipisci commodi?</p>
                                </div>
                            </div>
                       </div>
                   </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="page-footer font-small pt-4">
        <div class="footer-copyright text-center orange py-3 text-white">© 2020 Copyright:
            <!-- <a href="https://majapahit.id">Majapahit.id</a> -->
        </div>
    </footer>
        
