<div class="container my-4">
        <div class="row justify-content-center">
            <div class="col-8 card login">
               <div class="card-title mt-3 text-center "><h3>Login</h3></div>
               <div class="card-body">
                   <form action="" method="post" class="row">
                   <input type="hidden" id="base" value="<?php echo base_url(); ?>">
                        <div class="form-group col-12">
                            <label for="email">Email</label>
                            <input type="text" name="email" placeholder="youremail@email.com" autocomplete="off" class="form-control bulet" id="email">
                        </div>
                        <div class="form-group col-12">
                            <label for="pass">Password</label>
                            <input type="password" name="password" placeholder="mustbe 8-character" class="form-control bulet" id="user">
                        </div>
                        <div class="col-12">
                            <button type="button" id="login" name="login" class="btn btn-re btn-block">Login</button>
                        </div>
                        <div class="col-12">
                           <small>Don't Have Account ? <a href="<?=base_url() ?>/home/register">Register.</a></small>
                        </div>
                   </form>
               </div>
            </div>
        </div>
    </div>