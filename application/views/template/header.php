        <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand d-lg-none d-xl-none d-md-none d-sm-block " href="<?= base_url() ?>">Ecomercy</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav ml-auto">
                        <a class="nav-item nav-link active" href="<?= base_url('home') ?>">Home</a>
                        <a class="nav-item nav-link d-lg-none d-xl-none d-sm-block" href="<?= base_url('doc/index') ?>">Token</a>
                        <a class="nav-item nav-link d-lg-none d-xl-none d-sm-block" href="<?= base_url('doc/product') ?>">Product</a>
                        <?php if ($this->sess->isAuth()) : ?>
                            <a class="nav-item nav-link btn btn-danger btn-sm text-white" href="<?= base_url('home/logout') ?>">Logout</a>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </nav>
        <div class="wrapper">
            <nav id="sidebar" class="fixed-top">
                <div class="sidebar-header">
                    <a href="<?= base_url('home') ?>" class="side-title">
                        <h4 class="text-white">EcoMercy</h4>
                    </a>
                </div>
                <ul class="list-unstyled component">
                    <?php if ($this->sess->isAuth()) : ?>
                        <li class="active"><a href="<?= base_url('doc') ?>" class="btn-block">Token</a></li>
                    <?php endif; ?>
                    <li class="active"><a href="<?= base_url('doc/product') ?>" class="btn-block">Product</a></li>
                </ul>
            </nav>